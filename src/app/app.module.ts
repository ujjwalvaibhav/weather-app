import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { RouterModule, Routes, Router } from '@angular/router';
import { WeatherService } from './weather/weather.service';
import { HttpClientModule } from '@angular/common/http';
import { ChartModule } from 'angular-highcharts';

const appRoutes: Routes = [
  {
    path: 'weather',
    component: WeatherComponent,
    data: { title: 'Weather Forecast' }
  },
  { path: '',
    redirectTo: '/weather',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ChartModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    WeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
