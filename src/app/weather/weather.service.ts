import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http: HttpClient) { }

  /**
   * Method to get the weather data for
   * multiple cities by city id.
   * @param cities Comma separated ids of cities
  */
  getGroupWeather(cities: string) {
    return this.http.get(
      `http://api.openweathermap.org/data/2.5/group?id=${cities}&units=metric&APPID=11946cf6d94219196e01aeee68dbe86d`
    );
  }

  /**
   * Method to get weather data for single city by city id
   * @param cityId
   */
  getCityWeather(cityId: number) {
    return this.http.get(
      `http://api.openweathermap.org/data/2.5/forecast?id=${cityId}&units=metric&APPID=11946cf6d94219196e01aeee68dbe86d`
    );
  }

  /**
   * This method will create a digital clock in
   * 12HR format using recursive approach
   */
  showTime() {
    const date = new Date();
    let h: string | number = date.getHours(); // 0 - 23
    let m: string | number = date.getMinutes(); // 0 - 59
    let s: string | number = date.getSeconds(); // 0 - 59
    let session = 'AM';

    if (h === 0) {
      h = 12;
    }

    if (h > 12) {
      h = h - 12;
      session = 'PM';
    }

    h = (h < 10) ? '0' + h : h;
    m = (m < 10) ? '0' + m : m;
    s = (s < 10) ? '0' + s : s;

    const time = h + ':' + m + ':' + s + ' ' + session;
    document.getElementById('MyClockDisplay').innerText = time;
    document.getElementById('MyClockDisplay').textContent = time;

    setTimeout(() => {
      this.showTime();
    }, 1000);
  }

}
