import { Component, OnInit } from '@angular/core';
import { WeatherService } from './weather.service';
import { Chart } from 'angular-highcharts';
import * as moment from 'moment';
import * as TYPE from './weather.type';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  cities = '2759794,2988507,2950159,3067696,2673730'; // City ids are taken from openweatherapi site
  weatherData = [];
  chart: Chart;
  selectedCity = '';
  timeData = [];
  temperatureData = [];
  showError = false;
  currentDate = moment().format('MM-DD-YYYY');

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.weatherService.showTime();
    this.getGroupWeather();
  }

  /**
   * This method will return a list of
   * all the cities with their weather forecast.
   * @output this.weatherData Array<object>
   */
  getGroupWeather() {
    this.weatherService.getGroupWeather(this.cities)
      .subscribe((res: TYPE.GroupWeatherForecastModel) => {
        if (res.cnt) {
          this.weatherData = res.list;
        }
      }, () => {
        this.ShowServerError();
      });
  }

  /**
   * This method will return average temperature of
   * the day by passing the day's weather data.
   * @param data <object>: Containing temperature
   */
  getAverageTemprature(tempData: TYPE.TempDataModel) {
    return (tempData.temp_max + tempData.temp_min) / 2;
  }

  /**
   * This method will get full weather data for a city,
   * and set data to display on chart and table.
   * @param cityId
   * @output this.timeData Array<string> : Containing time in 12HR format
   * @output temperatureData Array <number> : Containing round off
   *         values of temperature in degree celcius
   */
  getCityWeather(cityId: number) {
    this.weatherService.getCityWeather(cityId)
      .subscribe((res: TYPE.CityWeatherForecastModel) => {
        this.timeData = [];
        this.temperatureData = [];
        res.list.map((ele, i) => {
          const d = moment(ele.dt_txt).format('MM-DD-YYYY');

          // Setting data for displaying upto 4 records. Can be increased.
          // Checking date inside list must be equals to today's date
          if (d === this.currentDate && i < 4) {
            const time = moment(ele.dt_txt).format('hh:mm A');
            this.timeData.push(time);
            this.temperatureData.push(Math.round(ele.main.temp));
          }
        });
        this.selectedCity = res.city.name + ', ' + res.city.country;
        this.initChart(); // for initializing data of city selected in chart
      }, () => {
        this.ShowServerError(); // Shows error if api failed.
      });
  }

  /**
   * This will show a alert if somehow api fails to respond
   * The alert will be disappeared after 5 seconds
   * @output this.showError Boolean : To hide show the alert box in html
   */
  ShowServerError() {
    this.showError = true;
    setTimeout(() => {
      this.showError = false;
    }, 5000);
  }

  /**
   * This method has the configuration of chart we are using
   * We can set options according to the need and
   * it is highly configurable
   */
  initChart() {
    const chart = new Chart({
      chart: {
        type: 'line'
      },
      tooltip: {
        headerFormat: '<br/><span style="font-size: 13px">{point.key}</span><br/>',
        pointFormat: '{series.name}: <b>{point.y}</b><br/>'
      },
      xAxis: {
        categories: this.timeData
      },
      yAxis: {
        title: {
          text: 'Temperature (°C)'
        }
      },
      title: {
        text: `Full Day Weather Forecast - <strong>${this.selectedCity}</strong>`
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'area',
        name: 'Time',
        data: this.timeData,
      },
      {
        type: 'area',
        name: 'Temprature',
        data: this.temperatureData
      }]
    });
    this.chart = chart;
  }

}
