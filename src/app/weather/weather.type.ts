export interface GroupWeatherForecastModel {
    cnt: number;
    list: Array<Object>;
}

export interface CityWeatherForecastModel {
    cnt: number;
    city: {
        name: String,
        country: String
    };
    list: Array<any>;
}

export interface TempDataModel {
    temp_max: number;
    temp_min: number;
}
